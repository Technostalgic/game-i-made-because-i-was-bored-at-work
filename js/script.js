let ctx = null;
let timeElapsed = 0;
let keys = [];
let controls = {
	left: 37,
	right: 39,
	up: 38,
	down: 40,
	attack: 67,
	nextWeapon: 70,
	previousWeapon: 68
};

let plr = null;
let enemies = [];
let bullets = [];
let enemySpawnDelay = 0;

function init(){
	document.body.innerHTML = "";
	document.body.style = "background-color:black;color:white;text-align:center";
	let canvas = document.createElement("canvas");
	canvas.width = 512;
	canvas.height = 512;
	ctx = canvas.getContext('2d');
	
	document.body.appendChild(canvas);
	
	window.addEventListener('keydown', function(e){
		keys[e.keyCode] = true;
		//console.log(e.key + ": " + e.keyCode);
	});
	window.addEventListener('keyup', function(e){
		keys[e.keyCode] = false;
	});
	
	plr = new Player();
	
	step(ctx);
}

function step(ctx){
	
	let lastTime = timeElapsed;
	timeElapsed = performance.now() / 1000;
	let dt = timeElapsed - lastTime;
	
	update(dt);
	draw(ctx);
	
	requestAnimationFrame(
		function(){
			step(ctx);
		});
}

function drawCircle(pos, radius = 5, fill = "#fff"){
	ctx.fillStyle = fill;
	ctx.beginPath();
	ctx.arc(pos.x, pos.y, radius, 0, 2 * Math.PI);
	ctx.fill();
}

function spawnEnemies(dt){
	
	if(!plr.alive)
		return;
	
	if(enemySpawnDelay > 0){
		enemySpawnDelay -= dt;
		return;
	}
	
	let pos = Vec2.FromDirection(Math.random() * Math.PI * 2, 350).translate(new Vec2(256, 256));
	let enemy = new Enemy(pos);
	
	enemies.push(enemy);
	
	enemySpawnDelay += 5 / ((timeElapsed / 30) + 1);
}

function update(dt){
	
	if(plr.alive) plr.update(dt);
	
	for(let i = bullets.length - 1; i >= 0; i--){
		bullets[i].update(dt);
		if(!bullets[i].existsInWorld)
			bullets.splice(i,1);
	}
	for(let i = enemies.length - 1; i >= 0; i--){
		enemies[i].update(dt);
		if(!enemies[i].existsInWorld)
			enemies.splice(i,1);
	}
	
	spawnEnemies(dt);
}

function draw(ctx){
	ctx.fillStyle = "#012";
	ctx.fillRect(0, 0, 512, 512);
	
	for(let i = bullets.length - 1; i >= 0; i--){
		bullets[i].draw(ctx);
	}
	for(let i = enemies.length - 1; i >= 0; i--){
		enemies[i].draw(ctx);
	}
	
	if(plr.alive) plr.draw(ctx);
	
	drawHud(ctx);
}

function drawHud(ctx){
	
	ctx.fillStyle = "#fff";
	ctx.fillText("Weapon: " + plr.weapon.name, 0, 10);
	ctx.fillText("Kills: " + plr.score, 0, 20)
}

window.onload = init;

class Vec2{
	constructor(x = 0, y = x){
		this.x = x;
		this.y = y;
	}
	
	translate(vec){
		return new Vec2(this.x + vec.x, this.y + vec.y);
	}
	scale(factor){
		return new Vec2(this.x * factor, this.y * factor);
	}
	normalized(magnitude = 1){
		return Vec2.FromDirection(this.direction, magnitude);
	}
	
	get clone(){
		return new Vec2(this.x, this.y);
	}
	get opposite(){
		return new Vec2(-this.x, -this.y);
	}
	
	get magnitude(){
		return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
	}
	get direction(){
		return Math.atan2(this.y, this.x);
	}
	
	static FromDirection(direction, magnitude = 1){
		
		let vec = new Vec2();
		vec.x = Math.cos(direction) * magnitude;
		vec.y = Math.sin(direction) * magnitude;
		
		return vec;
	}
}

class Weapon{
	constructor(){
		this.name = "Weapon";
		this.isEmpty = false;
	}
	
	update(dt){}
	
	use(){}
	
	unequip(){}
	
	drawHud(ctx){}
}

class Dagger extends Weapon{
	constructor(){
		super();
		
		this.name = "Dagger";
		this.projectiles = [];
		this.length = 15;
		this.tipDelta = 0;
		this.recoverSpeed = 1;
	}
	
	get tipPosition(){
		return plr.position.translate(Vec2.FromDirection(plr.aim) * length);
	}
	
	handleRecovery(dt){
		
		if(this.projectiles.length < 1){
			let p = new Bullet(plr.position);
			p.size = 3;
			this.projectiles.push(p);
			bullets.push(p);
			this.tipDelta = 0;
		}
		
		if(this.tipDelta < 1){
			this.tipDelta += dt * this.recoverSpeed;
			
			if(this.tipDelta >= 1)
				this.tipDelta = 1;
		}
	}
	
	handleProjectiles(){
		
		for(let i = this.projectiles.length - 1; i >= 0; i--){
			let add = Vec2.FromDirection(plr.aim, this.tipDelta * this.length);
			this.projectiles[i].position = plr.position.translate(add);
			
			if(!this.projectiles[i].isAlive)
				this.projectiles.splice(i, 1);
		}
	}
	
	unequip(){
		for(let i = this.projectiles.length - 1; i >= 0; i--){
			this.projectiles[i].remove();
		}
		this.projectiles = [];
	}
	
	update(dt){
		this.handleRecovery(dt);
		this.handleProjectiles();
	}
}

class Gun extends Weapon{
	constructor(){
		super();
		
		this.fireWait = 0;
		this.name = "Handgun";
		this.fireDelay = 0.5;
		this.projectileSpeed = 500;
		this.spread = 0.1;
	}
	
	use(){
		if(this.fireWait > 0)
			return;
		
		let bul = new Bullet(plr.position.clone);
		bul.velocity = Vec2.FromDirection(plr.aim + ((Math.random() - 0.5) * this.spread), 500);
		bullets.push(bul);
		
		this.fireWait += this.fireDelay;
	}
	
	update(dt){
		if(this.fireWait > 0)
			this.fireWait -= dt;
		else{
		
			if(this.fireWait < 0)
				this.fireWait = 0;
		}
	}
	
	drawHud(ctx){
		drawCircle(plr.position.translate(Vec2.FromDirection(plr.aim, 15)), 2, "#0f4");
	}
}

class Shotgun extends Gun{
	constructor(){
		super();
		
		this.name = "Shotgun";
		this.shots = 5;
		this.spread = 0.75;
		this.projectileSpeed = 350;
		this.fireDelay = 1;
	}
	
	use(){
		if(this.fireWait > 0)
			return;
		
		for(let i = this.shots - 1; i >= 0; i--){
			this.fireWait = 0;
			super.use();
		}
	}
}

class PhysObject{
	constructor(pos = new Vec2()){
		this.position = pos;
		this.velocity = new Vec2();
		this.size = 5;
		this.fill = "#fff";
		this.existsInWorld = true;
	}
	
	update(dt){
		this.position = this.position.translate(this.velocity.scale(dt));
	}
	draw(ctx){ 
		drawCircle(this.position, this.size, this.fill);
	}
	
	remove(){
		this.existsInWorld = false;
	}
	
	overlaps(physob){
		let dif = this.position.translate(physob.position.opposite);
		return dif.magnitude <= (this.size + physob.size);
	}
	outOfBounds(){
		return (
			this.position.x - this.size < 0 ||
			this.position.x + this.size > 512 ||
			this.position.y - this.size < 0 ||
			this.position.y + this.size > 512 );
	}
}

class Player extends PhysObject{
	constructor(){
		super(new Vec2(256));
		this.health = 10;
		this.aim = 0;
		this.speed = 100;
		this.weapons = [new Dagger(), new Gun(), new Shotgun()];
		this.selectedWeapon = 0;
		
		this.fireDelay = 0;
		this.size = 10;
		this.fill = "#0af";
		this.alive = true;
		this.canSwitchWeapon = true;
		
		this.score = 0;
	}
	
	get weapon(){
		return this.weapons[this.selectedWeapon];
	}
	
	switchWeapon(dir){
		dir = Math.sign(dir);
		
		this.weapon.unequip();
		this.selectedWeapon += dir;
		
		if(this.selectedWeapon < 0)
			this.selectedWeapon = this.weapons.length - 1;
		else if (this.selectedWeapon >= this.weapons.length)
			this.selectedWeapon = 0;
	}
	
	update(dt){
		super.update(dt);
		
		let switchWepDir = 0;
		if(keys[controls.nextWeapon])
			switchWepDir += 1;
		if(keys[controls.previousWeapon])
			switchWepDir -= 1;
		
		if(switchWepDir != 0){
			if(this.canSwitchWeapon){
				this.switchWeapon(switchWepDir);
				this.canSwitchWeapon = false;
			}
		}
		else{
			this.canSwitchWeapon = true;
		}
		
		let xm = 0;
		let ym = 0;
		
		if(keys[controls.left]) xm -= 1;
		if(keys[controls.right]) xm += 1;
		if(keys[controls.up]) ym -= 1;
		if(keys[controls.down]) ym += 1;
		
		xm *= this.speed;
		ym *= this.speed;
		
		this.velocity.x = xm;
		this.velocity.y = ym;
		
		if(this.velocity.magnitude > 0.1)
			this.aim = this.velocity.direction;
		
		this.weapon.update(dt);
		if(keys[controls.attack])
			this.weapon.use();
		
		if(this.outOfBounds()){
			this.velocity = new Vec2();
			
			if(this.position.x - this.size < 0)
				this.position.x = 0 + this.size;
			else if(this.position.x + this.size > 512)
				this.position.x = 512 - this.size;
			
			if(this.position.y - this.size < 0)
				this.position.y = 0 + this.size;
			else if(this.position.y + this.size > 512)
				this.position.y = 512 - this.size;
		}
	}
	
	draw(ctx){
		super.draw(ctx);
		this.weapon.drawHud(ctx);
	}
}

class Bullet extends PhysObject{
	constructor(pos = new Vec2()){
		super(pos);
		
		this.size = 4;
		this.fill = "#fff";
	}
	
	get isAlive(){
		return this.existsInWorld;
	}
	
	update(dt){
		super.update(dt);
		
		if(this.outOfBounds()){
			this.remove();
			return;
		}
		
		for(let i = enemies.length - 1; i >= 0; i--){
			if(this.overlaps(enemies[i])){
				this.remove();
				enemies[i].kill();
				break;
			}
		}
	}
}

class Enemy extends PhysObject{
	constructor(pos){
		super(pos);
		
		this.speed = 50;
		this.size = 12;
		this.fill = "#f40";
		this.points = 1;
	}
	
	kill(){
		plr.score += this.points;
		this.remove();
	}
	
	update(dt){
		super.update(dt);
		this.velocity = plr.position.translate(this.position.opposite).normalized(this.speed);
		
		if(this.outOfBounds()){
			this.velocity = new Vec2(256).translate(this.position.opposite).normalized(this.speed);
		}
		
		if(this.overlaps(plr)){
			plr.alive = false;
		}
	}
}